package 도형;

import 도형.Figure;
import 도형.hap;

public class Game_rule 
{
	Figure start[];
	public hap hap_info;
	int a;
	int b;
	int c;
	public Game_rule()
	{
		start=new Figure[9];
		hap_info=new hap();
	}
	public boolean decision(Figure one,Figure two, Figure three)
	{
		a=one.bgColor+two.bgColor+three.bgColor;
		b=one.figure+two.figure+three.figure;
		c=one.figure_color+two.figure_color+three.figure_color;
		
		if(a%3==0 && b%3==0 && c%3==0)
		return true;
		
		return false;
	}
	public int gameSetting(Figure one,Figure two,Figure three,Figure four,Figure five,Figure six,Figure seven,Figure eight,Figure nine)
	{
		start[0]=one;
		start[1]=two;
		start[2]=three;
		start[3]=four;
		start[4]=five;
		start[5]=six;
		start[6]=seven;
		start[7]=eight;
		start[8]=nine;
		
		for(int i=0; i<9; i++)
		{
			for(int j=i+1; j<9; j++)
			{
				for(int k=j+1; k<9; k++)
				{
					if(decision(start[i],start[j],start[k]))
					{
						hap_info.index[hap_info.hap_count][0]=i;
						hap_info.index[hap_info.hap_count][1]=j;
						hap_info.index[hap_info.hap_count][2]=k;
						hap_info.hap_count++;
					}
				}
			}
		}
		return hap_info.hap_count;
	}
}
